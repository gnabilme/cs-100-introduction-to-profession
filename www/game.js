var player;

var canvas = document.getElementById('game');

var context = canvas.getContext('2d');

var blockImage = document.createElement('img');
blockImage.src = 'http://www.mariocastle.it/w/images/thumb/6/63/Brick_Block.png/200px-Brick_Block.png';

var player = { x: 1, y: 1 };

var playerImage = document.createElement('img');
playerImage.src = 'http://photos-h.ak.instagram.com/hphotos-ak-xaf1/10584603_1461103157482935_1549714768_a.jpg';

var treasureImage = document.createElement('img'); 
treasureImage.src = 'http://www.wpclipart.com/money/treasure/treasure_2/treasure_chest_gold.png';

// draws the game map; values in the matrix are interpreted as follows:
//   - 0 : empty space
//   - 1 : a "wall"
//   - 2 : the "treasure"
//   - 3 : a player
//   - 4 : the winning player position
//
//  the current player position is also drawn on the map
var draw = function (map) {
  canvas.width = canvas.width; // clear the canvas

  var gridHeight = map.length;
  var gridWidth  = map[0].length;

  var cellWidth = canvas.width / gridWidth;
  var cellHeight = canvas.height / gridHeight;

  // draw the map
  for (var row=0; row<map.length; row++) {
    for (var col=0; col<map[row].length; col++) {
      if (map[row][col] !== 0) {
        switch (map[row][col]) {
        case 1: 
          context.drawImage(blockImage,
                            cellWidth * col,
                            cellHeight * row,
                            cellWidth,
                            cellHeight);
          break;
        case 2:
          context.drawImage(treasureImage,
                    cellWidth * col,
                    cellHeight * row,
                    cellWidth,
                    cellHeight);
          break;
        case 3:
          context.drawImage(playerImage, 
                    cellWidth * col,
                    cellHeight * row,
                    cellWidth,
                    cellHeight); 
          break;
        case 4: 
          context.fillStyle = 'blue';
	  context.fillRect(cellWidth * col, cellHeight * row,
                           cellWidth, cellHeight);
          break;
        }

      }
    }
  }

  // draw the player
    context.drawImage(playerImage, 
                    cellWidth * player.x,
                    cellHeight * player.y,
                    cellWidth,
                    cellHeight);

  // outline the map
  context.strokeRect(0, 0, canvas.width, canvas.height);
};

var socket = io(); // establish a connection to the server

// on receiving a "game update" message from the server, redraw the map
socket.on('game update', function (data) {
  // if we are given a new player position, keep track of it
  if (data.pos) {
    player = data.pos;
  }
  draw(data.map);
});

// each supported keystroke sends a message to the server
var keyHandler = function (key) {
  switch (key.keyCode) {
  case 37: // left
    socket.emit('player move', { dx: -1, dy:  0 });
    break;
  case 38: // up
    socket.emit('player move', { dx:  0, dy: -1 });
    break;
  case 39: // right
    socket.emit('player move', { dx:  1, dy:  0 });
    break;
  case 40: // down
    socket.emit('player move', { dx:  0, dy:  1 });
    break;
  case 82: // 'r' -- randomize the map
    socket.emit('randomize map', true);
  }
  key.preventDefault(); // prevent cursor keys from scrolling the browser viewport
};

// listen for key presses in the browser window
window.addEventListener('keydown', keyHandler);


// I've won! Draw message, disconnect from server, stop processing keys.
socket.on('win', function () {
  context.font = '36pt Helvetica';
  context.fillStyle = 'green';
  context.fillText('You win!', 35, 150);
  window.removeEventListener(keyHandler);
  socket.disconnect();
});

// I've lost! Draw message, disconnect from server, stop processing keys.
socket.on('lose', function (data) {
  draw(data.map);
  context.font = '36pt Helvetica';
  context.fillStyle = 'red';
  context.fillText('You lose!', 28, 150);
  window.removeEventListener(keyHandler);
  socket.disconnect();
});
